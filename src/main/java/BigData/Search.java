package BigData;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Search {
	
	//Pinali's Creds
	static String consumerKey = "QKCorYlld6PagNfAnnPAt4RXk";
	static String consumerSecret = "GjYZlxhMQODU2z6X55gdgkrRjXvNOdVRIcatkSrexMuwlbSbQi";
	static String token = "2836042628-eaJE7hZci1xZtitach9g481Zn0ZBRwdZRmRZI0j";
	static String tokenSecret = "nt1Xgt7C68gzOK8EW7TZHdpQJ37zFaxPJYvdFA2UjT87D";
	
	//Ruchi's creds
//	static String consumerKey = "ytycGiMPEycfWTEG0P4aToRK8";
//	static String consumerSecret = "lbqECdiPQCeBIQ5xuORdqeoe4fI7qMlKpCe605bU0d1jQOz852";
//	static String token = "125691571-wMtL8sCBdCPQMVLHi4e53FF1su5lSsuZC4ovA49A";
//	static String tokenSecret = "dQMJKY7cUXUu7BvFZe43pC7RqvJqCp7qxM985Q62Pilu2";
	
	@SuppressWarnings({ "deprecation", "resource" })
   public static void main(String[] args) throws ClientProtocolException, IOException, OAuthMessageSignerException, 
	   OAuthExpectationFailedException, OAuthCommunicationException, JSONException, InterruptedException{
	 
		ArrayList<String> searchKeys = new ArrayList<String>();
		if(args.length==0){  //getting command line arguments 
			searchKeys.add("apple");
			searchKeys.add("samsung");
			searchKeys.add("sony");		
		} else{
			for(int s=0; s<args.length;s++){  searchKeys.add(args[s]); }
		}
		String request=null;
		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(consumerKey, consumerSecret);
		List<Tweets> tweetList = new ArrayList<Tweets>();
		consumer.setTokenWithSecret(token, tokenSecret);	
	
		JSONObject oj, outputJS = new JSONObject();
		String retSrc=null, location=null;
		Tweets tw;
		JSONArray tokenList;
		HttpClient httpClient1= null;
		for (int i =0; i<searchKeys.size() ;  i++){
			request="https://api.twitter.com/1.1/search/tweets.json?q=%40" + searchKeys.get(i) + "&count=15";
			System.out.println(request);
			 httpClient1 = new DefaultHttpClient();
		//	HttpGet Request1 = new HttpGet("https://api.twitter.com/1.1/search/tweets.json?q=%40apple");
			HttpGet Request1 = new HttpGet(request);
		    consumer.sign(Request1);
			HttpResponse Response1 = httpClient1.execute(Request1);
			System.out.println("Request: "  + Response1.getStatusLine());	
			HttpEntity entity1 = Response1.getEntity();
			 if (entity1 != null) {
		    	 retSrc = EntityUtils.toString(entity1); 
		    	 tokenList = (new JSONObject(retSrc)).getJSONArray("statuses");
		    	 for(int k=0; k<tokenList.length(); k++){
		    		 oj = tokenList.getJSONObject(k);
		    		 tw=new Tweets();
		    		 tw.setId(oj.getString("id"));
		    		 tw.setSearchKey(searchKeys.get(i));
		    		 tw.setDateCreated(oj.getString("created_at"));
		    		 tw.setUserName(oj.getJSONObject("user").getString("screen_name"));
		    		 location=oj.getJSONObject("user").getString("location");
		    		 if(location.equals(null) || location.length()<=0){
		    			 location=getRandomLocation();
		    			 System.out.println(location);
		    			 
		    		 }
		    		 tw.setUserLocation(location);
		    		 tw.setDescription(oj.getString("text"));
		    		 tw.setPlaceName(oj.getString("place"));
		//    		 tw.setPlaceName(oj.getJSONObject("place").getString("full_name"));
		//    		 tw.setCountry(oj.getJSONObject("place").getString("country"));
		    		 tweetList.add(tw);
		    	 }	 
			 } 				 
	}
		
		
   httpClient1.getConnectionManager().shutdown();
  Collections.shuffle(tweetList);
	for(Tweets t : tweetList){
		outputJS.put("Index", tweetList.indexOf(t));
		outputJS.put("Key", t.getSearchKey());
		outputJS.put("User_Name", t.getUserName());
		outputJS.put("TimeStamp", t.getDateCreated());
		outputJS.put("Tweet_Id", t.getId());
		outputJS.put("TweetText", t.getDescription());
		outputJS.put("Location", t.getUserLocation());
		outputJS.put("Place", t.getPlaceName());
		//outputJS.put("Country", t.getCountry());
		System.out.println(outputJS);
		Thread.sleep(1000);
		
	}
    System.out.println("Total tweets:" + tweetList.size());
	}
	
	
	public static String getRandomLocation(){
		
		Set<String> l = new HashSet<String>();
		l.add("Scotland");
		l.add("India");
		l.add("Jakarta");
		l.add("America");
		l.add("Canada");
		l.add("China");
		l.add("Japan");
		/*Random ran = new Random();
		int x = ran.nextInt(l.size()+1);*/
		int item = new Random().nextInt(l.size()); 
		int i = 0;
		for(String obj : l)
		{
		    if (i == item)
		        return obj;
		    i = i + 1;
		}
		return null;
	}
}
   

